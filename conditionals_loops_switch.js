function getGrade(score) {
    let grade = "";
    if(score >= 0 && score <= 30){
        grade = "F";
    } else if(score >= 31 && score <= 45){
        grade = "D";
    } else if(score >= 46 && score <= 65){
        grade = "C";
    } else if(score >= 66 && score <= 84){
        grade = "B";
    } else if(score >= 85 && score <= 100){
        grade = "A";
    }
    return grade;
}
  
// console.log(getGrade(66))

function getAverage(ageArray) {
    let average = 0;

    for(let i = 0; i < ageArray.length; i++) {
        average += ageArray[i];
    }
    let avg = average / ageArray.length;
    return avg;
}

// console.log(getAverage([7, 7, 7, 7, 7, 8, 9, 10]));


function getDayName(dayNumber) {
    // dayNumber should only be between 1-7 (including both). 
    switch (dayNumber) {
        case 1:
            console.log("Monday");
            break;
        case 2:
            console.log("Tuesday");
            break;
        case 3:
            console.log("Wednesday");
            break;
        case 4:
            console.log("Thursday");
            break;
        case 5:
            console.log("Friday");
            break;
        case 6:
        case 7:
            console.log("It is weekend");
            break;
        default:
            console.log("Invalid day number");
            break;
    }
}
  
console.log(getGrade(86));
console.log(getGrade(40));
  
console.log(getAverage([12, 55, 64, 34, 90, 53, 29]));
console.log(getAverage([4, 6, 29, 68, 44]));
  
getDayName(3);
getDayName(7);
getDayName(19);