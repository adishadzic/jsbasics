console.log("Section 1")
const name  = "Adis";
console.log(`1 - ${name}`);

let surname = "";
console.log(`2 - ${surname}`);

const age = 22;
console.log(`3 - ${age}`);

let number = 7.77;
console.log(`4 - ${number}`);

const isMale = true;
console.log(`5 - ${isMale}`);

let isUnderage = false;
console.log(`6 - ${isUnderage}`);

let favFood;
console.log(`7 - ${favFood}`);

let favDrink = null;
console.log(`8 - ${favDrink}`);
console.log("********")

console.log("Section 2");
console.log(
    `${typeof(name)}`, '\n', `${typeof(surname)}`, '\n', 
    `${typeof(age)}`, '\n', `${typeof(number)}`, '\n', 
    `${typeof(isMale)}`, '\n', `${typeof(isUnderage)}`, '\n',
    `${typeof(favFood)}`, '\n', `${typeof(favDrink)}`
)
console.log("********")


console.log("Section 3");
const resultOfSum = 2 + 2;
const resultOfSubtraction = 2 - 2;
const resultOfMultiplication = 2 * 2;
const resultOfDivision = 2 / 2;

console.log(
    `${resultOfSum}`,'\n',`${resultOfSubtraction}`,'\n',`${resultOfMultiplication}`,'\n',`${resultOfDivision}`
);
console.log("********")

console.log("Section 4");

let text = "";
text = "Galatasaray is my favourite club";
console.log(text)
console.log("********")


console.log("Section 5");
const ageToString = age.toString();
console.log(ageToString);

console.log(favDrink == favFood); //true
console.log(favDrink === favFood); //false