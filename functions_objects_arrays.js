function factorial(int) {
  if (int < 0) return undefined;
  else if (int == 0) return 1;
  else {
    return int * factorial(int - 1);
  }
}

console.log(factorial(-10));

let userInfo = {
  name: 'Adis',
  birthdate: 817380271407120847,
  gender: 'M',
  hasAPet: true,
  numberOfCountriesVisited: 8,
  address: address.address,
};

// let cloneOne = {};
// for(let key in userInfo){
//     cloneOne[key] = userInfo[key];
// }
const cloneOne = { ...userInfo };
// console.log(cloneOne)

let cloneNamedDifferent = { ...userInfo };
cloneNamedDifferent.name = 'Elvis';
cloneNamedDifferent.address = address.address;

// console.log(cloneNamedDifferent)

console.log(userInfo === cloneOne);

let address = {
  address: 'Scalierova 1',
};

console.log(userInfo.address === cloneNamedDifferent.address);

function deleteField(array, string) {
  const newArr = [];
  for (let i = 0; i < array.length; ++i) {
    const newObj = { ...array[i] };
    delete newObj[string];
    newArr.push(newObj);
  }
  return newArr;
}
